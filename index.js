//  @author    Guillaume Rand - Gui
// @copyright Guillaume Rand 2019
// @license   Guilaume Rand - Tous droits réservés


window.onload = () =>{
	let span = document.querySelectorAll("span");
	for(let i=0; i<span.length; i++){	
		let color = i*360/9;
		span[i].style.borderColor = "hsl("+color+", 50%, 50%)";
	}
}
